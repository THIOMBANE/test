import kotlin.random.Random

fun main(args: Array<String>) {
   val myFirstDice = Dice(6)
    println("Your ${myFirstDice.numSides} sided dice rolled ${myFirstDice.roll()} !")

    val secondDice = Dice(20)
    println("Your ${secondDice.numSides} sided dice rolled ${secondDice.roll()} !")

}